# Fake Satellite Dataset

In order to begin work on a pipeline to process 3D files for machine learning before enough scans of actual debris can be collected a fake dataset has been produced.

## Satellite Parts From:

- https://grabcad.com/library/airbus-space-debris-removal-competition-entry-1
- https://grabcad.com/library/6u-cubesat-model-1
- https://grabcad.com/library/satellite-turksat-5a-1
- https://grabcad.com/library/cubic-satellite-1u-1
- https://grabcad.com/library/lumid-cubesat-1
- https://grabcad.com/library/cubesat-module-1
- https://grabcad.com/library/cubesat-jchang-1
- https://grabcad.com/library/venus-satellite-1
- https://grabcad.com/library/3u-cubesat
